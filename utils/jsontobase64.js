module.exports = json => {
    json = JSON.stringify(json);
    var base64 = Buffer.from(json).toString("base64");
    return base64;
  };
  