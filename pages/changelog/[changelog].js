import fetch from 'node-fetch'
const ReactMarkdown = require('react-markdown')

function Index({ changelog }) {
    return (
        <ReactMarkdown source={changelog} />
    )
}

export async function getServerSideProps(context) {
    if (context.params.changelog == "stable") {
        const res = await fetch('https://pastebin.com/raw/TgSkpNXm')
        const text = await res.text()
        return {
            props: {
                changelog: text,
            },
        }
    } else if (context.params.changelog == "dev") {
        const res = await fetch('https://pastebin.com/raw/TgSkpNXm')
        const text = await res.text()
        return {
            props: {
                changelog: text,
            },
        }
    } else {
        return {
            props: {
                changelog: "Error404",
            },
        }
    }

}



export default Index
