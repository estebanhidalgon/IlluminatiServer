import { Button } from '@material-ui/core';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    }
}));

function Index() {
    const classes = useStyles();
    return    <React.Fragment>

        <AppBar position="static">
            <Toolbar>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/">
                        IlluminatiMC
      </Link>
                    <Link color="inherit" href="/dashboard">
                        Dashboard
      </Link>
                    <span>
                        Cambiar Skin
      </span>
                </Breadcrumbs>
            </Toolbar>
        </AppBar>

        <input
            accept="image/*"
            style={{ display: 'none' }}
            id="raised-button-file"
            multiple
            type="file"
        />
        <label htmlFor="raised-button-file">
            <Button variant="raised" component="span">
                Upload
</Button>
        </label>     </React.Fragment>

}

export default Index