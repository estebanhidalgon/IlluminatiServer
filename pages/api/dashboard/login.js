var Airtable = require("airtable");
var base = new Airtable({
    apiKey: process.env.AIRTABLE_API_KEY
}).base(process.env.AIRTABLE_BASE_ID);

export default async (req, res) => {

    var logrado = false;
    base("Table 1")
        .select({
            maxRecords: 50,
            view: "Grid view"
        })
        .eachPage(
            function page(records, fetchNextPage) {
                records.forEach(function (record) {
                    if (
                        record.fields.username == req.body.username &&
                        record.fields.password == req.body.password
                    ) {
                        logrado = true;
                        res.setHeader(
                            "Set-Cookie",
                            `token=${record.fields.accessToken}; HttpOnly`
                        );
                        res.send("Login");
                    }
                });
                fetchNextPage();
            },
            async function done(err) {
                if (!logrado) {
                    res.status(403).send({ error: "User or password fail" })
                }
            }
        );


}