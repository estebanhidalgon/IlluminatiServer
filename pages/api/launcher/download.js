var Airtable = require("airtable");
var base = new Airtable({
    apiKey: process.env.AIRTABLE_API_KEY
}).base(process.env.AIRTABLE_BASE_ID_RELEASE);


export default (req, res) => {
    var logrado = false;
    var channels = {}
    base("Update")
        .select({
            maxRecords: 50,
            view: "Grid view"
        })
        .eachPage(
            function page(records, fetchNextPage) {

                records.forEach(function (record) {
                    if (record.fields.Channel == req.query.channel) {
                        if (req.query.platform == "win") {
                            if (record.fields.exe[0].url) {
                                logrado = true
                                res.redirect(record.fields.exe[0].url)
                            }
                        } else if (req.query.platform == "jar") {
                            if (record.fields.jar[0].url) {
                                logrado = true
                                res.redirect(record.fields.jar[0].url)
                            }
                        }
                    }
                });


                fetchNextPage();
            },
            function done(err) {
                if (!logrado) {
                    res.status(404).send({
                        error: "NotFoundChannelOrPlatform",
                        errorMessage: "Not found channel or platform in the database."
                    })

                }

            }
        );
}

