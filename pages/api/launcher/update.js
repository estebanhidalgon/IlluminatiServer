var Airtable = require("airtable");
var base = new Airtable({
  apiKey: process.env.AIRTABLE_API_KEY
}).base(process.env.AIRTABLE_BASE_ID_RELEASE);


export default (req, res) => {
  var logrado = false;
  var channels = {}
  base("Update")
    .select({
      maxRecords: 50,
      view: "Grid view"
    })
    .eachPage(
      function page(records, fetchNextPage) {

        records.forEach(function (record) {
          if (record.fields.Channel == req.query.channel) {
            logrado = true
            res.send({
              pack: record.fields.pack[0].url,
              packsha1: record.fields.packsha1,
              packxz: record.fields.packxz[0].url,
              packxzsha1: record.fields.packxzsha1,
              universal: record.fields.universal,
              version: record.fields.version
            })
          }
        });


        fetchNextPage();
      },
      function done(err) {
        if (!logrado) {
          res.status(404).send({
            error: "NotFoundChannel",
            errorMessage: "Not found channel in the database."
          })

        }

      }
    );
}

