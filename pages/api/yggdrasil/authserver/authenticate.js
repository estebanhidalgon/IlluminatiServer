var Airtable = require("airtable");
var base = new Airtable({
    apiKey: process.env.AIRTABLE_API_KEY
}).base(process.env.AIRTABLE_BASE_ID);

export default async (req, res) => {

    var logrado = false;
    base("Table 1")
        .select({
            maxRecords: 50,
            view: "Grid view"
        })
        .eachPage(
            function page(records, fetchNextPage) {
                records.forEach(function (record) {
                    if (
                        record.fields.username == req.body.username &&
                        record.fields.password == req.body.password
                    ) {
                        logrado = true;
                        var resp = {
                            accessToken: record.fields.accessToken,
                            clientToken: req.body.clientToken,
                            availableProfiles: [
                                {
                                    name: record.fields.nameProfile,
                                    id: record.fields.selectedProfile
                                }
                            ],
                            selectedProfile: {
                                name: record.fields.nameProfile,
                                id: record.fields.selectedProfile
                            },
                            user: {
                                properties: [],
                                id: record.fields.userId
                            }
                        };
                        console.log("Respuesta:", resp);
                        res.send(resp);
                    }
                });
                fetchNextPage();
            },
            async function done(err) {
                if (!logrado) {
                    const fetch = require('node-fetch');
                    var resp = await fetch('https://authserver.mojang.com/authenticate',
                        {
                            method: req.method,
                            body: JSON.stringify(req.body),
                            headers: { 'Content-Type': 'application/json' }
                        })
                    var json = await resp.json()
                    res.status(resp.status).send(json)
                }
            }
        );


}