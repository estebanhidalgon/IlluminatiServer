var Airtable = require("airtable");
var base = new Airtable({
    apiKey: process.env.AIRTABLE_API_KEY
}).base(process.env.AIRTABLE_BASE_ID);

export default (req, res) => {
    var logrado = false;
    base("Table 1")
        .select({
            maxRecords: 50,
            view: "Grid view"
        })
        .eachPage(
            function page(records, fetchNextPage) {
                records.forEach(function (record) {
                    if (record.fields.accessToken == req.body.accessToken) {
                        logrado = true;
                        console.log("Conseguido");
                        res.status(204).end();
                    }
                });
                fetchNextPage();
            },
            function done(err) {
                if (!logrado) {
                    const fetch = require('node-fetch');
                    var resp = await fetch('https://authserver.mojang.com/validate',
                        {
                            method: req.method,
                            body: JSON.stringify(req.body),
                            headers: { 'Content-Type': 'application/json' }
                        })
                    try {
                        var json = await resp.json()
                        res.status(resp.status).send(json)
                    } catch (err) {
                        res.status(resp.status).end()
                    }
                }
            }
        );

}