var Airtable = require("airtable");
const JsonToBase = require("../../../../../../utils/jsontobase64")
var base = new Airtable({
    apiKey: process.env.AIRTABLE_API_KEY
}).base(process.env.AIRTABLE_BASE_ID);


export default (req, res) => {
    console.log(req)
    var logrado = false;
    base("Table 1")
        .select({
            maxRecords: 50,
            view: "Grid view"
        })
        .eachPage(
            function page(records, fetchNextPage) {
                records.forEach(function (record) {
                    if (record.fields.nameProfile == req.query.username) {
                        logrado = true;
                        var resp = {
                            name: record.fields.nameProfile,
                            id: record.fields.selectedProfile,
                            properties: [
                                {
                                    name: "textures",
                                    value: JsonToBase({
                                        textures: {
                                            SKIN: {
                                                metadata: { model: record.fields.model },
                                                url: record.fields.skin
                                            }
                                        },
                                        timestamp: (Date.now() / 1000).toString(),
                                        signatureRequired: false,
                                        profileName: record.fields.nameProfile,
                                        profileId: record.fields.selectedProfile
                                    })
                                }
                            ]
                        };
                        console.log(resp);
                        res.send(resp);
                    }
                });
                fetchNextPage();
            },
            async function done(err) {
                if (!logrado) {
                    const fetch = require('node-fetch');
                    const { URL, URLSearchParams } = require('url');
                    var url = new URL('https://sessionserver.mojang.com/session/minecraft/hasJoined')
                    url.search = new URLSearchParams(req.query).toString();
                    var resp = await fetch(url,
                        {
                            method: req.method,
                            headers: { 'Content-Type': 'application/json' }
                        })
                    try {
                        var json = await resp.json()
                        res.status(resp.status).send(json)
                    } catch (err) {
                        res.status(resp.status).end()
                    }
                }
            }
        );
}