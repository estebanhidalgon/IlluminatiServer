var Airtable = require("airtable");
var base = new Airtable({
  apiKey: process.env.AIRTABLE_API_KEY
}).base(process.env.AIRTABLE_BASE_ID);


export default (req, res) => {
  var logrado = false;
  base("Table 1")
    .select({
      maxRecords: 50,
      view: "Grid view"
    })
    .eachPage(
      function page(records, fetchNextPage) {
        records.forEach(function (record) {
          if (
            record.fields.accessToken == req.body.accessToken &&
            record.fields.selectedProfile == req.body.selectedProfile
          ) {
            logrado = true;
            res.status(204).end();
          }
        });
        fetchNextPage();
      },
      async function done(err) {
        if (!logrado) {
          const fetch = require('node-fetch');
          var resp = await fetch('https://sessionserver.mojang.com/session/minecraft/join',
            {
              method: req.method,
              body: JSON.stringify(req.body),
              headers: { 'Content-Type': 'application/json' }
            })
          res.status(resp.status).send(json)
        }
      }
    );
}