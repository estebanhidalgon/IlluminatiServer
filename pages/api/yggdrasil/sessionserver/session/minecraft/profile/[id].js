var Airtable = require("airtable");
var base = new Airtable({
    apiKey: process.env.AIRTABLE_API_KEY
}).base(process.env.AIRTABLE_BASE_ID);

const JsonToBase = require("../../../../../../../utils/jsontobase64")

export default (req, res) => {

    const {
        query: { id },
    } = req

    var logrado = false;
    base("Table 1")
        .select({
            maxRecords: 50,
            view: "Grid view"
        })
        .eachPage(
            function page(records, fetchNextPage) {
                records.forEach(function (record) {
                    if (record.fields.selectedProfile == id) {
                        logrado = true;
                        var resp = {
                            name: record.fields.nameProfile,
                            id: record.fields.selectedProfile,
                            properties: [
                                {
                                    name: "textures",
                                    value: JsonToBase({
                                        textures: {
                                            SKIN: {
                                                metadata: { model: record.fields.model },
                                                url: record.fields.skin
                                            }
                                        },
                                        timestamp: (Date.now()/1000).toString(),
                                        signatureRequired: false,
                                        profileName: record.fields.nameProfile,
                                        profileId: record.fields.selectedProfile
                                    })
                                }
                            ]
                        };
                        console.log(resp);
                        res.send(resp);
                    }
                });
                fetchNextPage();
            },
            function done(err) {
                if (!logrado) {
                    console.log(204, "Not Content");
                    res
                        .status(204)
                        .end();
                }
            }
        );
}