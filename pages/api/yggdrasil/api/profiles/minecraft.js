var Airtable = require("airtable");
var base = new Airtable({
    apiKey: process.env.AIRTABLE_API_KEY
}).base(process.env.AIRTABLE_BASE_ID);


export default (req, res) => {
    var resp = []
    base("Table 1")
        .select({
            maxRecords: 50,
            view: "Grid view"
        })
        .eachPage(
            function page(records, fetchNextPage) {
                req.body.forEach(function (username) {
                    records.forEach(function (record) {
                        if (record.fields.nameProfile.toLowerCase() == username.toLowerCase()) {
                            resp.push({
                                id: record.fields.selectedProfile,
                                name: record.fields.nameProfile
                            })

                        }
                    });
                })

                fetchNextPage();
            },
            function done(err) {
                console.log(resp)
                res.send(resp)

            }
        );
}