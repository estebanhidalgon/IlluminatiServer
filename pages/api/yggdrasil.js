export default (req, res) => {
    res.json({
        skinDomains: ["i.imgur.com", "imgur.com"],
        meta: {
            implementationVersion: "1.0.0",
            serverName: "The Illuminati Server Local",
            implementationName: "the-illuminati-server"
        }
    })
}
