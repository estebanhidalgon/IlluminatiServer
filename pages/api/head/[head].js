var Airtable = require("airtable");
var base = new Airtable({
    apiKey: process.env.AIRTABLE_API_KEY
}).base(process.env.AIRTABLE_BASE_ID);

var isArrayBufferSupported = (new Buffer(new Uint8Array([1]).buffer)[0] === 1);

var arrayBufferToBuffer = isArrayBufferSupported ? arrayBufferToBufferAsArgument : arrayBufferToBufferCycle;

function arrayBufferToBufferAsArgument(ab) {
    return new Buffer(ab);
}

function arrayBufferToBufferCycle(ab) {
    var buffer = new Buffer(ab.byteLength);
    var view = new Uint8Array(ab);
    for (var i = 0; i < buffer.length; ++i) {
        buffer[i] = view[i];
    }
    return buffer;
}

export default (req, res) => {
    const {
        query: { head },
    } = req
    var urlhead = `https://minotar.net/helm/${head}`
    var logrado = false;
    base("Table 1")
        .select({
            maxRecords: 50,
            view: "Grid view"
        })
        .eachPage(
            function page(records, fetchNextPage) {
                records.forEach(function (record) {
                    if (record.fields.nameProfile == head) {
                        if (record.fields.head) {
                            urlhead = record.fields.head
                        }
                    }
                });
                fetchNextPage();
            },
            async function done(err) {
                var resp = await fetch(urlhead)
                var image = await resp.arrayBuffer()
                var image = arrayBufferToBuffer(image)
                res.setHeader("Content-Type", 'image/png')
                res.send(image)
            }
        );
}